package com.sda.cinema.model;

public enum FilmGenre {
    ACTION, ADVENTURE, COMEDY, CRIME, DRAMA, FANTASY, HISTORICAL
}
